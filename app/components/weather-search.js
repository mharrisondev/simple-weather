/*global $ */
import Ember from 'ember';

export default Ember.Component.extend({
  weather: Ember.inject.service(),
  init() {
    this._super(...arguments);
    this.fetchLocations();
    this.set('weather.locationValue', {id: 2193734, name: 'Auckland'}); // Set default location
  },
  props: {
    loading: true
  },
  locations: null,
  fetchLocations: function() {
    var self = this;
    var nzData = [];
    $.getJSON("/data/curatedLocationsDB.json", function(data) {
      data.filter(function(loc) {
          if (loc.country === "NZ") {
            nzData.push({id: loc.id, name: loc.name});
          }
        return nzData;
      });
      console.log("nzData", nzData);
      self.set('props.loading', false);
      return self.set('locations', nzData);
    });
  },
  actions: {
    updateLocation(loc) {
      this.set('weather.locationValue', loc);
    }
  }
});
