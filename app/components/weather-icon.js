import Ember from 'ember';

export default Ember.Component.extend({
  tagName: 'i',
  classNameBindings: ['weatherIcon'],
  weatherIcon: Ember.computed('icon', function() {
    var newIcon;
    var iconId = this.get('icon') + ""; // stringify

    switch (iconId) {
      case "800": //clear sky
        newIcon = "wi-day-sunny"
        break;
      case "801": //few clouds
      case "802": //scattered clouds
      case "803": //broken clouds
        newIcon = "wi-day-cloudy"
        break;
      case "804": //overcast clouds
        newIcon = "wi-cloudy"
        break;
      case "200": //	thunderstorm with light rain
      case "201": //	thunderstorm with rain
      case "202": //	thunderstorm with heavy rain
      case "210": //	light thunderstorm
      case "211": //	thunderstorm
      case "212": //	heavy thunderstorm
      case "221": //	ragged thunderstorm
      case "230": //	thunderstorm with light drizzle
      case "231": //	thunderstorm with drizzle
      case "232": //	thunderstorm with heavy drizzle
        newIcon = "wi-day-thunderstorm"
        break;
      case "300": //light intensity drizzle
      case "301": //drizzle
      case "302": //heavy intensity drizzle
      case "310": //light intensity drizzle rain
      case "311": //drizzle rain
      case "312": //heavy intensity drizzle rain
      case "313": //shower rain and drizzle
      case "314": //heavy shower rain and drizzle
      case "321": //shower drizzle
        newIcon = "wi-day-showers"
        break;
      case "500": //light rain
      case "501": //moderate rain
      case "502": //heavy intensity rain
      case "503": //very heavy rain
      case "504": //extreme rain
        newIcon = "wi-day-rain"
        break;
      case "511": //freezing rain
      case "520": //light intensity shower rain
      case "521": //shower rain
      case "522": //heavy intensity shower rain
      case "531": //ragged shower rain
        newIcon = "wi-day-rain-wind"
        break;
      case "600":	//light snow
      case "601":	//snow
      case "602":	//heavy snow
      case "611":	//sleet
      case "612":	//shower sleet
      case "615":	//light rain and snow
      case "616":	//rain and snow
      case "620":	//light shower snow
      case "621":	//shower snow
      case "622":	//heavy shower snow
        newIcon = "wi-day-snow"
        break;
      case "701":	//mist
      case "711":	//smoke
      case "721":	//haze
      case "731":	//sand, dust whirls
      case "741":	//fog
      case "751":	//sand
      case "761":	//dust
        newIcon = "wi-dust"
        break;
      case "762":	//volcanic ash
        newIcon = "wi-volcano"
        break;
      case "771":	//squalls
        newIcon = "wi-strong-wind"
        break;
      case "781":	//tornado
        newIcon = "wi-tornado"
        break;
      case "900":	//tornado
        newIcon = "wi-tornado"
        break;
      case "901":	//tropical storm
        newIcon = "wi-storm-showers"
        break;
      case "902":	//hurricane
        newIcon = "wi-hurricane"
        break;
      case "903":	//cold
        newIcon = "wi-snowflake-cold"
        break;
      case "904":	//hot
        newIcon = "wi-hot"
        break;
      case "905":	//windy
        newIcon = "wi-windy"
        break;
      case "906":	//hail
        newIcon = "wi-hail"
        break;
      default:
        newIcon = "wi-moon-full"
    }

    return "wi " + newIcon;
  })
});
