import Ember from 'ember';
import moment from 'moment';

export default Ember.Component.extend({
  weather: Ember.inject.service(),
  forecast5: Ember.computed('weather.forecast5', function() {
    let rawData = this.get('weather.forecast5.list');
    // setup hash
    var hash = rawData.reduce(function(hash, value) {
      let dataDate = moment.unix(value.dt).format('DD-MM-YY');
      let dataDateHumanize = moment.unix(value.dt).format('ddd Do MMM');
      var existing = hash[dataDate];

      if (existing) {
        hash[dataDate].data.push(value);
      } else {
        hash[dataDate] = {date: dataDate, dateHumanize: dataDateHumanize, data: [value]};
      }
      return hash;
    }, new Ember.Object);

    return hash;
  })
});
