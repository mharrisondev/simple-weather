import Ember from 'ember';

export default Ember.Component.extend({
  tabs: Ember.computed('forecast5', function() {
    // console.log("'forecast-map-tabs' - forecastData", this.get('forecastData'));
    let forecastData = this.get('forecast5');
    let tabData = [];
    for (var key in forecastData) {
      if (forecastData.hasOwnProperty(key)) {
        // console.log(key, forecastData[key]);
        let newTab = {};

        newTab.id = forecastData[key].date;
        newTab.title = forecastData[key].dateHumanize;
        newTab.dayForecast = forecastData[key].data;
        tabData.push(newTab);
      }
    }

    // console.log("tabData", tabData);
    return tabData;
  })
});
