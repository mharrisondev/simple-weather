import Ember from 'ember';

export function roundFloat(params/*, hash*/) {
  return Math.round(params);
}

export default Ember.Helper.helper(roundFloat);
