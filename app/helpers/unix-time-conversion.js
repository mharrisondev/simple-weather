import Ember from 'ember';
import moment from 'moment';

export function unixTimeConversion(params/*, hash*/) {
  var unix = moment.unix(params[0]).format("hh:mm A");
  return unix;
}

export default Ember.Helper.helper(unixTimeConversion);
