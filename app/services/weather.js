import Ember from 'ember';

export default Ember.Service.extend({
  locationValue: {id: 2193734, name: 'Auckland'},
  refetch: function() {
    this.set('forecast', null);
    this.set('forecast5', null);
    var id = this.get('locationValue.id');
    fetch(`http://api.openweathermap.org/data/2.5/weather?units=metric&id=${id}&APPID=148d210ae67411a583776efb66973178`)
      .then( response => { return response.json(); })
      .then( data => {
        if (id == this.get('locationValue.id')) {
          this.set('forecast', data);
        }
      });

    fetch(`http://api.openweathermap.org/data/2.5/forecast?units=metric&id=${id}&APPID=148d210ae67411a583776efb66973178`)
      .then( response => { return response.json(); })
      .then( data => {
        if (id == this.get('locationValue.id')) {
          this.set('forecast5', data);
        }
      });
  }.observes('locationValue')

  // data: Ember.computed('locationValue', function() {
  //   console.log(`Fetching new data for ${this.get('locationValue.name')} id: ${this.get('locationValue.id')}`);
  //   var id = this.get('locationValue.id')
  //   /* Cities ID list - http://bulk.openweathermap.org/sample/city.list.json.gz */
  //   var dataUrls = [
  //     `http://api.openweathermap.org/data/2.5/weather?units=metric&id=${id}&APPID=148d210ae67411a583776efb66973178`,
  //     `http://api.openweathermap.org/data/2.5/forecast?units=metric&id=${id}&APPID=148d210ae67411a583776efb66973178`
  //   ];
  //   return Ember.RSVP.all(dataUrls.map(url =>
  //     fetch(url)
  //     .then(response => {
  //       return response.json();
  //     })
  //   ))
  // })

});
