
import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('unix-time-conversion', 'helper:unix-time-conversion', {
  integration: true
});

// Replace this with your real tests.
test('it renders', function(assert) {
  this.set('inputValue', '1234');

  this.render(hbs`{{unix-time-conversion inputValue}}`);

  assert.equal(this.$().text().trim(), '1234');
});

