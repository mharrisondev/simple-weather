require 'json'
json = JSON.parse(File.read('./public/data/locationsDB.json'))
nz = []
json.each do |location|
  nz << location if location["country"] == "NZ"
end
File.open("filtered.json", "w+") do |f|
  f.write nz.to_json
end
